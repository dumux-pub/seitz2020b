SUMMARY
=======
This is the DuMuX module containing the code for producing the results
of the PhD Thesis of Gabriele Seitz:

G. Seitz<br>
Modeling Fixed-Bed Reactors for Thermochemical Heat Storage with the Reaction System CaO/Ca(OH)2



Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir seitz2020b && cd seitz2020b
git clone https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020b.git
```

After that, execute the file [installSeitz2020b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020b/-/blob/master/installSeitz2020b.sh).
```
chmod +x seitz2020a/installSeitz2020b.sh
./seitz2020a/installSeitz2020b.sh
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run.

Applications
============

You can find the code for reproducing the simulations in the appl folder. To run them, navigate to:
```
cd seitz2020b/build-cmake/appl
```

For the results presented in chapters [4.3](https://git.iws.uni-stuttgart.de/dumux-pub/seitz2019b) and [5](https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020b), we refer to the pub-modules of the respective publications.



Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installSeitz2020b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020b/-/blob/master/installSeitz2020b.sh).
