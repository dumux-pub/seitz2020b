// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetTests
 * \brief Test for the one-phase facet coupling model.
 */
#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>

#include "problem_bulk.hh"
#include "problem_facet.hh"

using BulkTypeTag = Dumux::Properties::TTag::BULKTYPETAG;
using FacetTypeTag = Dumux::Properties::TTag::FACETTYPETAG;

namespace Dumux {

// obtain/define some types to be used below in the property definitions and in main
class TestTraits
{
public:
    using BulkGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    using FacetGridGeometry = GetPropType<FacetTypeTag, Properties::GridGeometry>;
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, FacetTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkGridGeometry, FacetGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// define coupling manager property in sub-problems
namespace Properties {
template<class TypeTag> struct CouplingManager<TypeTag, BulkTypeTag> { using type = typename TestTraits::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, FacetTypeTag> { using type = typename TestTraits::CouplingManager; };
} // end namespace Properties
} // end namespace Dumux

/*!
 * \brief Updates the finite volume grid geometry for the box scheme.
 *
 * This is necessary as the finite volume grid geometry for the box scheme with
 * facet coupling requires additional data for the update. The reason is that
 * we have to create additional faces on interior boundaries, which wouldn't be
 * created in the standard scheme.
 */
template< class GridGeometry,
          class GridManager,
          class FacetGridView,
          std::enable_if_t<GridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(GridGeometry& gridGeometry,
                              const GridManager& gridManager,
                              const FacetGridView& facetGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    gridGeometry.update(facetGridView, facetGridAdapter);
}

/*!
 * \brief Updates the finite volume grid geometry for the cell-centered schemes.
 */
template< class GridGeometry,
          class GridManager,
          class LowDimGridView,
          std::enable_if_t<GridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(GridGeometry& gridGeometry,
                              const GridManager& gridManager,
                              const LowDimGridView& lowDimGridView)
{
    gridGeometry.update();
}

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////
    using BulkGrid = GetPropType<BulkTypeTag, Properties::Grid>;
    using FacetGrid = GetPropType<FacetTypeTag, Properties::Grid>;
    using GridManager = FacetCouplingGridManager<BulkGrid, FacetGrid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& facetGridView = gridManager.template grid<1>().leafGridView();

    // create the finite volume grid geometries
    using BulkFVGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    using FacetFVGridGeometry = GetPropType<FacetTypeTag, Properties::GridGeometry>;
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    auto facetFvGridGeometry = std::make_shared<FacetFVGridGeometry>(facetGridView);
    updateBulkFVGridGeometry(*bulkFvGridGeometry, gridManager, facetGridView);
    facetFvGridGeometry->update();

    // the coupling mapper
    auto couplingMapper = std::make_shared<typename TestTraits::CouplingMapper>();
    couplingMapper->update(*bulkFvGridGeometry, *facetFvGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    using FacetProblem = GetPropType<FacetTypeTag, Properties::Problem>;
    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkFvGridGeometry, "Bulk");
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, bulkSpatialParams, couplingManager, "Bulk");
    auto facetSpatialParams = std::make_shared<typename FacetProblem::SpatialParams>(facetFvGridGeometry, "Facet");
    auto facetProblem = std::make_shared<FacetProblem>(facetFvGridGeometry, facetSpatialParams, couplingManager, "Facet");

    // the solution vector
    using MDTraits = typename TestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x, xOld;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto facetId = typename MDTraits::template SubDomain<1>::Index();
    x[bulkId].resize(bulkFvGridGeometry->numDofs());
    x[facetId].resize(facetFvGridGeometry->numDofs());
    bulkProblem->applyInitialSolution(x[bulkId]);
    facetProblem->applyInitialSolution(x[facetId]);
    xOld = x;

    // initialize coupling manager
    couplingManager->init(bulkProblem, facetProblem, couplingMapper, x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    using FacetGridVariables = GetPropType<FacetTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    auto facetGridVariables = std::make_shared<FacetGridVariables>(facetProblem, facetFvGridGeometry);
    bulkGridVariables->init(x[bulkId]);
    facetGridVariables->init(x[facetId]);

    // get some time loop parameters
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize");
    const auto episodeLength = getParam<double>("TimeLoop.EpisodeLength");
    auto dt = getParam<double>("TimeLoop.Dt");

//     const bool outputVtk = getParam<bool>("Problem.EnableVtkOutput", true);

    // intialize the vtk output module
    using BulkSolutionVector = std::decay_t<decltype(x[bulkId])>;
    using FacetSolutionVector = std::decay_t<decltype(x[facetId])>;
    VtkOutputModule<BulkGridVariables, BulkSolutionVector> bulkVtkWriter(*bulkGridVariables, x[bulkId], bulkProblem->name(), "Bulk");
    VtkOutputModule<FacetGridVariables, FacetSolutionVector> facetVtkWriter(*facetGridVariables, x[facetId], facetProblem->name(), "Facet");

    // Add model specific output fields
    using BulkIOFields = GetPropType<BulkTypeTag, Properties::IOFields>;
    using FacetIOFields = GetPropType<FacetTypeTag, Properties::IOFields>;
    BulkIOFields::initOutputModule(bulkVtkWriter);
    FacetIOFields::initOutputModule(facetVtkWriter);

    // Add model specific output fields
    bulkVtkWriter.addField(bulkProblem->getRRate(), "reactionRate");

    // write initial solution
    bulkVtkWriter.write(0.0);
    facetVtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<double>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, facetProblem),
                                                  std::make_tuple(bulkFvGridGeometry, facetFvGridGeometry),
                                                  std::make_tuple(bulkGridVariables, facetGridVariables),
                                                  couplingManager, timeLoop, xOld);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // time loop
    timeLoop->setPeriodicCheckPoint(episodeLength);
    timeLoop->start(); do
    {
        // linearize & solve
        newtonSolver->solve(x, *timeLoop);
        bulkProblem->setTimeStep( timeLoop->timeStepSize() );
        // make the new solution the old solution
        bulkGridVariables->advanceTimeStep();
        facetGridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // report statistics of this time step
        timeLoop->reportTimeStep();

        ////////////////////////////////////////////////////////////
        // compute volume fluxes for the bulk subproblem
        ////////////////////////////////////////////////////////////
        using NumEqVectorBulk = GetPropType<BulkTypeTag, Properties::NumEqVector>;
        using BulkSubControlVolume = typename TestTraits::BulkGridGeometry::SubControlVolume;
        using BulkGlobalPosition = typename BulkSubControlVolume::GlobalPosition;
        using LocalResidualBulk = GetPropType<BulkTypeTag, Properties::LocalResidual>;
        using ElementBoundaryTypes = GetPropType<BulkTypeTag, Properties::ElementBoundaryTypes>;
        LocalResidualBulk localResidualBulk(bulkProblem.get(), timeLoop.get());

        // find dof on inlet (Dirichlet BC)
//         static constexpr int dim = bulkGridView.dimension;
//         std::vector<bool> bulkIsOnInlet(bulkGridView.size(dim), false);
//         for (const auto& element : elements(bulkGridView))
//         {
//             auto fvGeometry = localView(*bulkFvGridGeometry);
//             fvGeometry.bind(element);
//
//             for (const auto& scv : scvs(fvGeometry))
//             {
//                 const auto& globalPosition = scv.center();
//                 if(globalPosition[2] <= 0.01)
//                 {
//                     if(fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex()))
//                     {
//                         for (const auto& scvf : scvfs(fvGeometry))
//                         {
// //                             const auto bcTypes = bulkProblem.boundaryTypes(element, scvf);
//                             /*if (bcTypes.hasDirichlet()) */bulkIsOnInlet[scv] = true;
//                         }
//                     }
//                 }
//             }
//         }

        NumEqVectorBulk inFluxBulk(0.0);
        NumEqVectorBulk storageBulk(0.0);
        NumEqVectorBulk sumReaction(0.0);
        for (const auto& element : elements(bulkGridView))
        {
            auto fvGeometry = localView(*bulkFvGridGeometry);
            fvGeometry.bind(element);

            auto elemVolVars = localView(bulkGridVariables->curGridVolVars());
            elemVolVars.bind(element, fvGeometry, x[bulkId]);
            auto prevElemVolVars = localView(bulkGridVariables->prevGridVolVars());
            prevElemVolVars.bind(element, fvGeometry, xOld[bulkId]);
            auto elemFluxVarsCache = localView(bulkGridVariables->gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            ElementBoundaryTypes elementBoundaryTypes;
            elementBoundaryTypes.update(*bulkProblem, element, fvGeometry);

            couplingManager->bindCouplingContext(bulkId, element, *assembler);

            //calculate storage:
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];

                const auto& globalPosition = scv.center();
                if(globalPosition[2] <= 0.005) //0.01
                {
                    if(fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex()))
                    {
                        auto temp = localResidualBulk.evalStorage(element, fvGeometry, prevElemVolVars, elemVolVars);
                        temp += localResidualBulk.evalFluxAndSource(element, fvGeometry, elemVolVars, elemFluxVarsCache, elementBoundaryTypes);
                        inFluxBulk+=temp[scv.localDofIndex()];
//                         std::cout << "inFluxBulk " << inFluxBulk<< "\n";
                    }
                }

                if(globalPosition[2] <= 0.08)
                {
                    storageBulk[0 /*N2*/] += volVars.molarDensity(0/*phaseIdx*/)*volVars.porosity()
                                                *volVars.moleFraction(0/*phaseIdx*/, 0 /*compIdx*/)
                                                *scv.volume();
                    storageBulk[1 /*H2O*/] += volVars.molarDensity(0/*phaseIdx*/)*volVars.porosity()
                                                *volVars.moleFraction(0/*phaseIdx*/, 1 /*compIdx*/)
                                                *scv.volume();
                    storageBulk[2 /*CaO*/] += volVars.solidVolumeFraction(0/*cPhaseIdx*/)
                                                * volVars.solidComponentMolarDensity(0/*cPhaseIdx*/)*scv.volume();
                    storageBulk[3 /*CaO2H2*/] += volVars.solidVolumeFraction(1/*hPhaseIdx*/)
                                                * volVars.solidComponentMolarDensity(1/*hPhaseIdx*/)*scv.volume();
                    storageBulk[4 /*energy*/] += (volVars.porosity()
                                                * volVars.density(0/*phaseIdx*/)
                                                * volVars.internalEnergy(0/*phaseIdx*/)
                                                +
                                                volVars.temperature()
                                                * volVars.solidHeatCapacity()
                                                * volVars.solidDensity()
                                                * (1.0 - volVars.porosity()))
                                                *scv.volume();

                    sumReaction += bulkProblem->source(element, fvGeometry, elemVolVars, scv)*scv.volume();

                }

            }
        }
        ////////////////////////////////////////////////////////////
        // compute volume fluxes for the facet subproblem
        ////////////////////////////////////////////////////////////
        using NumEqVectorFacet = GetPropType<FacetTypeTag, Properties::NumEqVector>;
        using FacetSubControlVolume = typename TestTraits::FacetGridGeometry::SubControlVolume;
        using FacetGlobalPosition = typename FacetSubControlVolume::GlobalPosition;
        using LocalResidualFacet = GetPropType<FacetTypeTag, Properties::LocalResidual>;
        using FacetElementBoundaryTypes = GetPropType<FacetTypeTag, Properties::ElementBoundaryTypes>;
        LocalResidualFacet localResidualFacet(facetProblem.get(), timeLoop.get());
        NumEqVectorFacet outFluxFacet(0.0);
        NumEqVectorFacet inFluxFacet(0.0);
        NumEqVectorFacet storageFacet(0.0);
        NumEqVectorFacet fluxFacetBulk(0.0);
        double fluxFacetBulkPlusH2O(0.0);
        double fluxFacetBulkPlusN2(0.0);
        double fluxFacetBulkPlusE(0.0);
        for (const auto& element : elements(facetGridView))
        {
            auto fvGeometry = localView(*facetFvGridGeometry);
            fvGeometry.bind(element);

            auto elemVolVars = localView(facetGridVariables->curGridVolVars());
            elemVolVars.bind(element, fvGeometry, x[facetId]);
            auto prevElemVolVars = localView(facetGridVariables->prevGridVolVars());
            prevElemVolVars.bind(element, fvGeometry, xOld[facetId]);
            auto elemFluxVarsCache = localView(facetGridVariables->gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            FacetElementBoundaryTypes elementBoundaryTypes;
            elementBoundaryTypes.update(*facetProblem, element, fvGeometry);

//             for (const auto& scvf : scvfs(fvGeometry))
//             {
//                 const auto& ipGlobal = scvf.ipGlobal();
//                 const auto& volVars = elemVolVars[scvf.insideScvIdx()];
//                 const auto& fluxVarsCache = elemFluxVarsCache[scvf];
// //                 // top (outflux) boundary
//                 double eps = 1e-6;
//                 if(ipGlobal[2] < 0.08 + eps && ipGlobal[2] > 0.08 - eps  )
//                 {
//                 }
//                 // bottom (influx) boundary
//                 if(ipGlobal[2] < 1e-6)
//                 {
//                 }
//             }
            couplingManager->bindCouplingContext(facetId, element, *assembler);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];

                const auto& globalPosition = scv.center();
                if(globalPosition[2] <= 0.001) //0.01
                {
                    if(fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex()))
                    {
                        auto temp = localResidualFacet.evalStorage(element, fvGeometry, prevElemVolVars, elemVolVars);
                        temp += localResidualFacet.evalFluxAndSource(element, fvGeometry, elemVolVars, elemFluxVarsCache, elementBoundaryTypes);
                        inFluxFacet+=temp[scv.localDofIndex()];
//                         std::cout << "inFluxFacet " << inFluxFacet<< "\n";
                    }
                }

                //calculate storage:
                if(globalPosition[2] <= 0.08)
                {
                    storageFacet[0 /*N2*/] += volVars.molarDensity(0/*phaseIdx*/)*volVars.porosity()
                                                *volVars.moleFraction(0/*phaseIdx*/, 0 /*compIdx*/)
                                                *scv.volume()*volVars.extrusionFactor();
                    storageFacet[1 /*H2O*/] += volVars.molarDensity(0/*phaseIdx*/)*volVars.porosity()
                                                *volVars.moleFraction(0/*phaseIdx*/, 1 /*compIdx*/)
                                                *scv.volume()*volVars.extrusionFactor();
                    storageFacet[2 /*CaO*/] += volVars.solidVolumeFraction(0/*cPhaseIdx*/)
                                                * volVars.solidComponentMolarDensity(0/*cPhaseIdx*/)*scv.volume()*volVars.extrusionFactor();
                    storageFacet[3 /*CaO2H2*/] += volVars.solidVolumeFraction(1/*hPhaseIdx*/)
                                                * volVars.solidComponentMolarDensity(1/*hPhaseIdx*/)*scv.volume()*volVars.extrusionFactor();
                    storageFacet[4 /*energy*/] += (volVars.porosity()
                                                * volVars.density(0/*phaseIdx*/)
                                                * volVars.internalEnergy(0/*phaseIdx*/)
                                                +
                                                volVars.temperature()
                                                * volVars.solidHeatCapacity()
                                                * volVars.solidDensity()
                                                * (1.0 - volVars.porosity()))
                                                *scv.volume()*volVars.extrusionFactor();
                }

                //calculate sum of the flux bulk - facet:
                //TODO check: scv is not on inlet (dofonBoundary für inlet bestimmen, excluden, weil die auch nicht in der massenbilanz auftauchen
                if(!fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex()))
                {
                    fluxFacetBulk += facetProblem->source(element, fvGeometry, elemVolVars, scv)*scv.volume()*volVars.extrusionFactor();

                    if (facetProblem->source(element, fvGeometry, elemVolVars, scv)[0] >= 0.0)
                        fluxFacetBulkPlusH2O += facetProblem->source(element, fvGeometry, elemVolVars, scv)[0]*scv.volume()*volVars.extrusionFactor();
                    if (facetProblem->source(element, fvGeometry, elemVolVars, scv)[1] >= 0.0)
                        fluxFacetBulkPlusN2 += facetProblem->source(element, fvGeometry, elemVolVars, scv)[1]*scv.volume()*volVars.extrusionFactor();
                   if (facetProblem->source(element, fvGeometry, elemVolVars, scv)[4] >= 0.0)
                        fluxFacetBulkPlusE += facetProblem->source(element, fvGeometry, elemVolVars, scv)[4]*scv.volume()*volVars.extrusionFactor();
                }
            }
        }

        xOld = x;

        // print out all the balances in the bulkproblem
        bulkProblem->outputBalances(storageBulk, storageFacet, fluxFacetBulk, sumReaction, inFluxBulk , inFluxFacet, fluxFacetBulkPlusH2O, fluxFacetBulkPlusN2, fluxFacetBulkPlusE);

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));

        // write vtk output
//         TODO uncomment if statement to write out only at the end of an episode
        if (timeLoop->isCheckPoint() || timeLoop->finished())
        {
            bulkVtkWriter.write(timeLoop->time());
            facetVtkWriter.write(timeLoop->time());

            bulkProblem->updateVtkOutput(x[bulkId]);
        }


    } while (!timeLoop->finished());

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
