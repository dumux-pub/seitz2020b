Merge "Risse12.brep";

// Physical domain definitions
Physical Volume(2) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.08;
Physical Volume(1) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.08;

// Physical entity definitions
Physical Surface(20) = {27};
Characteristic Length{ PointsOf{Surface{27};} } = 0.008;
Physical Surface(18) = {26};
Characteristic Length{ PointsOf{Surface{26};} } = 0.008;
Physical Surface(16) = {25};
Characteristic Length{ PointsOf{Surface{25};} } = 0.008;
Physical Surface(15) = {24};
Characteristic Length{ PointsOf{Surface{24};} } = 0.008;
Physical Surface(13) = {9};
Characteristic Length{ PointsOf{Surface{9};} } = 0.008;
Physical Surface(11) = {8};
Characteristic Length{ PointsOf{Surface{8};} } = 0.008;
Physical Surface(17) = {10};
Characteristic Length{ PointsOf{Surface{10};} } = 0.008;
Physical Surface(3) = {6, 7};
Characteristic Length{ PointsOf{Surface{6, 7};} } = 0.008;
Physical Surface(1) = {4};
Characteristic Length{ PointsOf{Surface{4};} } = 0.008;
Physical Surface(2) = {5};
Characteristic Length{ PointsOf{Surface{5};} } = 0.008;
Physical Surface(19) = {20};
Characteristic Length{ PointsOf{Surface{20};} } = 0.008;
Physical Surface(7) = {11, 12};
Characteristic Length{ PointsOf{Surface{11, 12};} } = 0.008;
Physical Surface(8) = {13, 15};
Characteristic Length{ PointsOf{Surface{13, 15};} } = 0.008;
Physical Surface(9) = {14};
Characteristic Length{ PointsOf{Surface{14};} } = 0.008;
Physical Surface(14) = {16};
Characteristic Length{ PointsOf{Surface{16};} } = 0.008;
Physical Surface(5) = {17};
Characteristic Length{ PointsOf{Surface{17};} } = 0.008;
Physical Surface(4) = {18, 19};
Characteristic Length{ PointsOf{Surface{18, 19};} } = 0.008;
Physical Surface(6) = {21};
Characteristic Length{ PointsOf{Surface{21};} } = 0.008;
Physical Surface(10) = {22};
Characteristic Length{ PointsOf{Surface{22};} } = 0.008;
Physical Surface(12) = {23};
Characteristic Length{ PointsOf{Surface{23};} } = 0.008;

// Physical entity intersections definitions
Physical Line(3) = {101};
Physical Line(7) = {95};
Physical Line(6) = {86};
Physical Line(17) = {75};
Physical Line(5) = {55};
Physical Line(12) = {54};
Physical Line(13) = {56};
Physical Line(9) = {63};
Physical Line(10) = {64};
Physical Line(4) = {71};
