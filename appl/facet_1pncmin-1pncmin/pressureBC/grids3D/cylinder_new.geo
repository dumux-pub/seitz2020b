meshSize = 0.5;

r1 = 0.055/2;
h1 = 0.02;

r2 = 0.055/2;
h2 = 0.02;

r3 = 0.055/2;
h3 = 0.02;

r4 = 0.055/2;
h4 = 0.02;

r5 = 0.055/2;
h5 = 0.02;

SetFactory("OpenCASCADE");
Cylinder(1) = {0, 0, 0, 0, 0, h1, r1, 2*Pi};
Cylinder(2) = {0, 0, h1, 0, 0, h2, r2, 2*Pi};
BooleanFragments{ Volume{1, 2}; Delete; }{ Volume{1, 2}; Delete; }

Cylinder(3) = {0, 0, h1+h2, 0, 0, h3, r3, 2*Pi};
BooleanFragments{ Volume{2, 3}; Delete; }{ Volume{2, 3}; Delete; }

Cylinder(4) = {0, 0, h1+h2+h3, 0, 0, h4, r3, 2*Pi};
BooleanFragments{ Volume{3, 4}; Delete; }{ Volume{3, 4}; Delete; }

Cylinder(5) = {0, 0, h1+h2+h3+h4, 0, 0, h5, r3, 2*Pi};
BooleanFragments{ Volume{4, 5}; Delete; }{ Volume{4, 5}; Delete; }

Characteristic Length{ PointsOf{ Volume{:}; } } = meshSize;
