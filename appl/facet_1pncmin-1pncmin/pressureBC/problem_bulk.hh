// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetTests
 * \brief The problem for the bulk domain in the 1pnc facet coupling test.
 */
#ifndef DUMUX_TEST_TPFAFACETCOUPLING_ONEPNC_BULKPROBLEM_HH
#define DUMUX_TEST_TPFAFACETCOUPLING_ONEPNC_BULKPROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/material/fluidsystems/h2on2.hh>
#include <dumux/material/fluidsystems/1padapter.hh>

#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1pncmin/model.hh>

#include <dumux/material/fluidmatrixinteractions/1p/thermalconductivityaverage.hh>
#include <dumux/material/components/cao2h2.hh>
#include "cao.hh"
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include "spatialparams_bulk.hh"
#include "thermochemreaction.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePNCBulkProblem;

namespace Properties {

// create the type tag nodes
namespace TTag {
struct OnePNCBulk { using InheritsFrom = std::tuple<OnePNCMinNI>; };
// struct OnePNCBulkTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePNCBulk>; };
// struct OnePNCBulkMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, OnePNCBulk>; };
struct OnePNCBulkBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePNCBulk>; };
} // end namespace TTag

// Set the grid type (DIMWORLD is defined in CMakeLists.txt)
template<class TypeTag>
// old struct
// struct Grid<TypeTag, TTag::OnePNCBulk> { using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };
// use this with 3D grids
struct Grid<TypeTag, TTag::OnePNCBulk> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePNCBulk> { using type = OnePNCBulkProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePNCBulk>
{
    using type = OnePNCMinNIBulkSpatialParams< GetPropType<TypeTag, Properties::GridGeometry>,
                                    GetPropType<TypeTag, Properties::Scalar> >;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePNCBulk>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2ON2 = FluidSystems::H2ON2<Scalar, FluidSystems::H2ON2DefaultPolicy</*simplified=*/true>>;
public:
    using type = FluidSystems::OnePAdapter<H2ON2, H2ON2::gasPhaseIdx>;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::OnePNCBulk>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Components::CaO<Scalar>;
    using ComponentTwo = Components::CaO2H2<Scalar>;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo>;
};

} // end namespace Properties

/*!
 * \ingroup FacetTests
 * \brief Test problem for the 1pnc model with
 *        coupling across the bulk grid facets.
 */
template<class TypeTag>
class OnePNCBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using ReactionRate = ThermoChemReactionPoro;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

public:
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    enum
    {
        // Indices of the primary variables
        pressureIdx = Indices::pressureIdx,
//         N2Idx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::N2Idx),

        H2OIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::H2OIdx),
        CaOIdx = FluidSystem::numComponents,
        CaO2H2Idx = FluidSystem::numComponents+1,

        // Equation Indices
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase Indices
        phaseIdx = FluidSystem::phase0Idx,
        cPhaseIdx = SolidSystem::comp0Idx,

        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx
    };

    OnePNCBulkProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                      std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                      std::shared_ptr<CouplingManager> couplingManager,
                      const std::string& paramGroup = "")
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {
        //initialize fluid system
        //initialize fluid system
        FluidSystem::init(/*tempMin=*/363.15,
                          /*tempMax=*/823.15,
                          /*numTemptempSteps=*/30,
                          /*startPressure=*/1e5,
                          /*endPressure=*/3e5,
                          /*pressureSteps=*/100);

        // stating in the console whether mole or mass fractions are used
        if(getPropValue<TypeTag, Properties::UseMoles>())
            std::cout << "problem uses mole fractions" << std::endl;
        else
            DUNE_THROW(Dune::InvalidStateException, "Problem is implemented for molar formulation!");

        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" +
                         getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        inletPressure_=getParam<Scalar>("Problem.InletPressure");
        reactionRate_.resize(gridGeometry->numDofs());
        fractureAperture_=getParam<Scalar>("Problem.FractureAperture");
        outputFile_.open(problemName_ + ".csv", std::ios::out);
        outputFile_ << "timeStep [s], inFluxBulkN2 [mol], inFluxBulkH2O [mol], inFluxBulkEnthalpy [J],  inFluxFacetN2 [mol], inFluxFacetH2O [mol], inFluxFacetEnthalpy [J], fluxFacetBulkN2 [mol], fluxFacetBulkH2O [mol],  fluxFacetBulkEnthalpy [J], storageBulkN2 [mol], storageBulkH2O [mol], storageBulkCaO [mol], storageBulkCaOH2 [mol], storageBulkEnergy [J], storageFacetN2 [mol], storageFacetH2O [mol], storageFacetEnergy [J], sumReactionH2O [mol], sumReactionEnergy [J],  fluxFacetBulkH2OPlus [mol], fluxFacetBulkN2Plus [mol], fluxFacetBulkEPlus [mol]"  << std::endl;
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Get the time step of the time loop
    void setTimeStep(Scalar t)
    { timeStep_ = t; }

    //! Specifies the type of boundary condition at a given position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (globalPos[2] < 1e-6 || globalPos[2] > this->gridGeometry().bBoxMax()[2] - 1e-6){
            values.setDirichlet(pressureIdx);
            values.setDirichlet(H2OIdx);
            values.setDirichlet(temperatureIdx);
//                         values.setAllDirichlet();
        }
//             values.setAllDirichlet();
        return values;
    }

    //! Specifies the type of interior boundary condition at a given position.
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Evaluates the source term at a given position.
    NumEqVector source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {

        ////// reaction source
        NumEqVector source(0.0);
        const auto& volVars = elemVolVars[scv];

        Scalar qMass = rrate_.thermoChemReactionSimple(volVars);
        Scalar  qMole =  qMass/FluidSystem::molarMass(H2OIdx)*(1-volVars.porosity());

//     //    make sure not more solid reacts than present
//     //    for CaO
       if (-qMole*timeStep_ + volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx) < 0 + eps_)
        {
            qMole = volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx)/timeStep_;
        }

        source[CaO2H2Idx] = qMole;
        source[CaOIdx] = - qMole;
        source[H2OIdx] = - qMole;

        Scalar deltaH = 108e3; // J/mol
        Scalar phi = volVars.porosity();
        source[energyEqIdx] = qMole * (deltaH - (phi/(1-phi))*(volVars.pressure(phaseIdx)/volVars.molarDensity(phaseIdx)));

        return source;
    }

    //! Evaluates the Dirichlet boundary condition for a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);

        if (globalPos[2] < 1e-6){
            values[temperatureIdx] = 573.15;
            values[H2OIdx] = 0.5;
            values[pressureIdx] = inletPressure_ ;//4.0e5;// 2.4e5;
        }

        if (globalPos[2] > this->gridGeometry().bBoxMax()[2] - 1e-6 ){
            values[temperatureIdx] = 573.15;
            values[H2OIdx] = 0.5;
            values[pressureIdx] = 2.0e5;
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache The cache related to flux computation
     * \param scvf The sub-control volume face
     *
     * For this method, the \a values parameter stores the flux
     * in normal direction of each phase. Negative values mean influx.
     * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
//     NumEqVector neumann(const Element& element,
//                         const FVElementGeometry& fvGeometry,
//                         const ElementVolumeVariables& elemVolVars,
//                         const ElementFluxVariablesCache& elemFluxVarsCache,
//                         const SubControlVolumeFace& scvf) const
//     {
//
//         NumEqVector flux(0.0);
//
//         // set a fixed pressure on the right side of the domain
//         const auto& ipGlobal = scvf.ipGlobal();
//         const auto zMin = this->gridGeometry().bBoxMin()[2];
//         const auto& volVars = elemVolVars[scvf.insideScvIdx()];
//         const auto& fluxVarsCache = elemFluxVarsCache[scvf];
//         const Scalar dirichletPressure = 2.4e5;
//
//         FluidState fluidStateBorder;
//         fluidStateBorder.setTemperature(573.15);
//         fluidStateBorder.setPressure(phaseIdx, 2.1e5);
//         fluidStateBorder.setMoleFraction(phaseIdx, pressureIdx, 0.5);
//         fluidStateBorder.setMoleFraction(phaseIdx, H2OIdx, 0.5);
//         fluidStateBorder.setMassFraction(phaseIdx, pressureIdx, 0.4375);
//         fluidStateBorder.setMassFraction(phaseIdx, H2OIdx, 0.5625);
//
//         if(ipGlobal[2] < 0.0 + eps_ )
//         {
//             auto density = FluidSystem::density(fluidStateBorder, phaseIdx);
//             auto molarDensity = FluidSystem::molarDensity(fluidStateBorder, phaseIdx);
//             auto enthalpy = FluidSystem::enthalpy(fluidStateBorder, phaseIdx);
//
//             // evaluate the gradient
//             GlobalPosition gradP(0.0);
//             for (const auto& scv : scvs(fvGeometry))
//             {
//                 const auto zIp = scv.dofPosition()[2];
//                 auto tmp = fluxVarsCache.gradN(scv.localDofIndex());
//                 tmp *= zIp < zMin + eps_ ? dirichletPressure
//                                         : elemVolVars[scv].pressure();
//                 gradP += tmp;
//             }
//             // calculate the volume flux
//             Scalar volumeFlux = vtmv(scvf.unitOuterNormal(), volVars.permeability(), gradP) * volVars.mobility();
//
//             flux[pressureIdx] = volumeFlux * molarDensity * 0.5 /*moleFraction N2*/;
//             flux[H2OIdx] = volumeFlux * molarDensity * 0.5 /*moleFraction H2O*/;
//             flux[temperatureIdx] = volumeFlux * density * enthalpy;
//         }
//
//        return flux;
//     }

    //! Evaluates the initial conditions.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        values[pressureIdx] = 2.0e5;
        values[H2OIdx] = 0.5;
        values[CaOIdx] = 0.2;
        values[CaO2H2Idx] = 0.0;
        values[temperatureIdx] = 573.15;

        if (globalPos[2] > 0.08 ){
            values[CaO2H2Idx] = 0.01;
            values[CaOIdx] = 0.0001;
        }


        return values;
    }

    //! Returns the temperature in \f$\mathrm{[K]}\f$ in the domain.
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! Returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

     /*!
     * \brief Return the reaction rate
     */
    const std::vector<Scalar>& getRRate()
    {
        return reactionRate_;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                reactionRate_[dofIdxGlobal] = (1-volVars.porosity())*rrate_.thermoChemReactionSimple(volVars);

            }
        }
    }

    //! Called after every time step
    //! Output the total out flux balances in [ \textnormal{unit of conserved quantity} )]
    void outputBalances(NumEqVector& storageBulk, NumEqVector& storageFacet, NumEqVector& fluxFacetBulk, NumEqVector& sumReaction, NumEqVector& inFluxBulk, NumEqVector& inFluxFacet, Scalar fluxFacetBulkPlusH2O, Scalar fluxFacetBulkPlusN2, Scalar fluxFacetBulkPlusE )
    {
        outputFile_ << timeStep_<<", "
                    << inFluxBulk[pressureIdx] *timeStep_<<", "
                    << inFluxBulk[H2OIdx] *timeStep_<<", "
                    << inFluxBulk[temperatureIdx]*timeStep_ <<", "
                    << inFluxFacet[pressureIdx]*fractureAperture_*timeStep_<<", "
                    << inFluxFacet[H2OIdx]*fractureAperture_*timeStep_<<", "
                    << inFluxFacet[temperatureIdx]*fractureAperture_*timeStep_<<", "
                    << fluxFacetBulk[pressureIdx]*timeStep_<<", "
                    << fluxFacetBulk[H2OIdx]*timeStep_<<", "
                    << fluxFacetBulk[temperatureIdx]*timeStep_<<", "
                    << storageBulk[pressureIdx] <<", "
                    << storageBulk[H2OIdx] <<", "
                    << storageBulk[CaOIdx] <<", "
                    << storageBulk[CaO2H2Idx] <<", "
                    << storageBulk[temperatureIdx]<<", "
                    << storageFacet[pressureIdx] <<", "
                    << storageFacet[H2OIdx] <<", "
                    << storageFacet[temperatureIdx] <<", "
                    << sumReaction[H2OIdx]*timeStep_ <<", "
                    << sumReaction[temperatureIdx]*timeStep_<<", "
                    << fluxFacetBulkPlusH2O*timeStep_<<", "
                    << fluxFacetBulkPlusN2*timeStep_<<", "
                    << fluxFacetBulkPlusE*timeStep_
                    <<std::endl;
    }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
    ReactionRate rrate_;
    Scalar timeStep_;
    Scalar eps_ = 1.0e-6;
    std::ofstream outputFile_;
    std::vector<double> reactionRate_;
    Scalar fractureAperture_;
    Scalar inletPressure_;
};

} // end namespace Dumux

#endif
