// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetTests
 * \brief The spatial parameters for the single-phase facet coupling test.
 */

#ifndef DUMUX_TEST_FACETCOUPLING_ONEPNCMINNI_SPATIALPARAMS_BULK_HH
#define DUMUX_TEST_FACETCOUPLING_ONEPNCMINNI_SPATIALPARAMS_BULK_HH

#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \ingroup FacetTests
 * \brief The spatial parameters for the single-phase facet coupling test.
 */
template< class GridGeometry, class Scalar >
class OnePNCMinNIBulkSpatialParams
: public FVSpatialParamsOneP< GridGeometry, Scalar, OnePNCMinNIBulkSpatialParams<GridGeometry, Scalar> >
{
    using ThisType = OnePNCMinNIBulkSpatialParams< GridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< GridGeometry, Scalar, ThisType >;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

public:
    //! Export the type used for permeabilities
    using PermeabilityType = Scalar;

    OnePNCMinNIBulkSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry, const std::string& paramGroup = "")
    : ParentType(gridGeometry)
    {
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "Bulk.SpatialParams.Permeability");
        permeabilityFacet_ = getParamFromGroup<Scalar>(paramGroup, "Facet.SpatialParams.Permeability");
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const auto& globalPosition = scv.center();
        if (globalPosition[2] > 0.1 ){
            return permeabilityFacet_;
        }

        auto priVars = evalSolution(element, element.geometry(), elemSol, scv.center());
        Scalar sumPrecipitates = 0.0;
        for (unsigned int solidPhaseIdx = 0; solidPhaseIdx < 2; ++solidPhaseIdx)
        sumPrecipitates += priVars[/*numComp*/2 + solidPhaseIdx];

        using std::max;
        const auto poro = 1 - sumPrecipitates;

        Scalar referencePermeability = permeability_;

        return /*evaluatePermeability(referencePermeability, 0.8, poro); //*/permeability_;

    }


    //Kozeny-Carman
    PermeabilityType evaluatePermeability(PermeabilityType refPerm, Scalar refPoro, Scalar poro) const
    {
        using std::pow;
        auto factor = pow((1.0 - refPoro)/(1.0 - poro), 2) * pow(poro/refPoro, 3);
        refPerm *= factor;
        return refPerm;
    }

private:
    PermeabilityType permeability_;
    PermeabilityType permeabilityFacet_;
    Scalar porosity_;
};

} // end namespace Dumux

#endif
