Merge "Risse12.brep";

// Physical domain definitions
Physical Volume(1) = {3};
Characteristic Length{ PointsOf{Volume{3};} } = 0.04;
Physical Volume(2) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.04;
Physical Volume(3) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.04;

// Physical entity definitions
Physical Surface(17) = {32};
Characteristic Length{ PointsOf{Surface{32};} } = 0.008;
Physical Surface(15) = {31};
Characteristic Length{ PointsOf{Surface{31};} } = 0.008;
Physical Surface(9) = {30};
Characteristic Length{ PointsOf{Surface{30};} } = 0.008;
Physical Surface(7) = {29};
Characteristic Length{ PointsOf{Surface{29};} } = 0.008;
Physical Surface(6) = {28};
Characteristic Length{ PointsOf{Surface{28};} } = 0.008;
Physical Surface(5) = {27};
Characteristic Length{ PointsOf{Surface{27};} } = 0.008;
Physical Surface(4) = {26};
Characteristic Length{ PointsOf{Surface{26};} } = 0.008;
Physical Surface(20) = {24};
Characteristic Length{ PointsOf{Surface{24};} } = 0.008;
Physical Surface(18) = {22, 23, 25};
Characteristic Length{ PointsOf{Surface{22, 23, 25};} } = 0.008;
Physical Surface(8) = {9};
Characteristic Length{ PointsOf{Surface{9};} } = 0.008;
Physical Surface(19) = {8};
Characteristic Length{ PointsOf{Surface{8};} } = 0.008;
Physical Surface(11) = {10, 11};
Characteristic Length{ PointsOf{Surface{10, 11};} } = 0.008;
Physical Surface(1) = {4, 5, 7};
Characteristic Length{ PointsOf{Surface{4, 5, 7};} } = 0.008;
Physical Surface(12) = {6};
Characteristic Length{ PointsOf{Surface{6};} } = 0.008;
Physical Surface(14) = {12};
Characteristic Length{ PointsOf{Surface{12};} } = 0.008;
Physical Surface(3) = {18};
Characteristic Length{ PointsOf{Surface{18};} } = 0.008;
Physical Surface(16) = {13, 14};
Characteristic Length{ PointsOf{Surface{13, 14};} } = 0.008;
Physical Surface(2) = {15, 16};
Characteristic Length{ PointsOf{Surface{15, 16};} } = 0.008;
Physical Surface(13) = {17, 19};
Characteristic Length{ PointsOf{Surface{17, 19};} } = 0.008;
Physical Surface(10) = {20, 21};
Characteristic Length{ PointsOf{Surface{20, 21};} } = 0.008;

// Physical entity intersections definitions
Physical Line(8) = {116};
Physical Line(16) = {111};
Physical Line(25) = {58};
Physical Line(14) = {99};
Physical Line(4) = {66};
Physical Line(3) = {73};
Physical Line(26) = {56};
Physical Line(6) = {79};
Physical Line(7) = {80};
Physical Line(5) = {81};
Physical Line(10) = {96};
Physical Line(21) = {110};
Physical Line(13) = {104};
