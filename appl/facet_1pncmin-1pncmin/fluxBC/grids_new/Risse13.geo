Merge "Risse12.brep";

// Physical domain definitions
Physical Volume(3) = {4};
Characteristic Length{ PointsOf{Volume{4};} } = 0.04;
Physical Volume(4) = {3};
Characteristic Length{ PointsOf{Volume{3};} } = 0.04;
Physical Volume(2) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.04;
Physical Volume(1) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.04;

// Physical entity definitions
Physical Surface(18) = {31};
Characteristic Length{ PointsOf{Surface{31};} } = 0.008;
Physical Surface(17) = {30};
Characteristic Length{ PointsOf{Surface{30};} } = 0.008;
Physical Surface(13) = {29};
Characteristic Length{ PointsOf{Surface{29};} } = 0.008;
Physical Surface(12) = {28};
Characteristic Length{ PointsOf{Surface{28};} } = 0.008;
Physical Surface(10) = {27};
Characteristic Length{ PointsOf{Surface{27};} } = 0.008;
Physical Surface(9) = {26};
Characteristic Length{ PointsOf{Surface{26};} } = 0.008;
Physical Surface(11) = {25};
Characteristic Length{ PointsOf{Surface{25};} } = 0.008;
Physical Surface(8) = {24};
Characteristic Length{ PointsOf{Surface{24};} } = 0.008;
Physical Surface(19) = {23};
Characteristic Length{ PointsOf{Surface{23};} } = 0.008;
Physical Surface(15) = {14, 15};
Characteristic Length{ PointsOf{Surface{14, 15};} } = 0.008;
Physical Surface(4) = {20};
Characteristic Length{ PointsOf{Surface{20};} } = 0.008;
Physical Surface(16) = {16};
Characteristic Length{ PointsOf{Surface{16};} } = 0.008;
Physical Surface(5) = {22};
Characteristic Length{ PointsOf{Surface{22};} } = 0.008;
Physical Surface(6) = {12, 13};
Characteristic Length{ PointsOf{Surface{12, 13};} } = 0.008;
Physical Surface(1) = {10};
Characteristic Length{ PointsOf{Surface{10};} } = 0.008;
Physical Surface(14) = {11};
Characteristic Length{ PointsOf{Surface{11};} } = 0.008;
Physical Surface(3) = {19};
Characteristic Length{ PointsOf{Surface{19};} } = 0.008;
Physical Surface(7) = {17};
Characteristic Length{ PointsOf{Surface{17};} } = 0.008;
Physical Surface(2) = {18};
Characteristic Length{ PointsOf{Surface{18};} } = 0.008;
Physical Surface(20) = {21};
Characteristic Length{ PointsOf{Surface{21};} } = 0.008;

// Physical entity intersections definitions
Physical Line(4) = {104};
Physical Line(8) = {96};
Physical Line(2) = {73};
Physical Line(13) = {54};
Physical Line(3) = {61};
Physical Line(1) = {88};
Physical Line(12) = {49};
Physical Line(7) = {68};
Physical Line(11) = {60};
