// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetTests
 * \brief Test for the one-phase facet coupling model.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

//  #include <dumux/assembly/diffmethod.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include "problem_ohneRiss.hh"


// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TYPETAG;

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& gridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(gridView);
    gridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(gridGeometry->numDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // get some time loop parameters
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize");
    const auto episodeLength = getParam<double>("TimeLoop.EpisodeLength");
    auto dt = getParam<double>("TimeLoop.Dt");

    // intialize the vtk output module
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    IOFields::initOutputModule(vtkWriter);

    // Add model specific output fields
    vtkWriter.addField(problem->getRRate(), "reactionRate");

    // update the output fields before writing initial solution
    problem->updateVtkOutput(x);
    vtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<double>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // time loop
    timeLoop->setPeriodicCheckPoint(episodeLength);
    timeLoop->start(); do
    {
        // linearize & solve
        nonLinearSolver.solve(x, *timeLoop);
        problem->setTimeStep( timeLoop->timeStepSize() );

        // make the new solution the old solution
        gridVariables->advanceTimeStep();


        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // report statistics of this time step
        timeLoop->reportTimeStep();

        ////////////////////////////////////////////////////////////
        // compute volume fluxes for the bulk subproblem
        ////////////////////////////////////////////////////////////
        using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
//         using BulkSubControlVolume = typename TestTraits::GridGeometry::SubControlVolume;
//         using BulkGlobalPosition = typename SubControlVolume::GlobalPosition;
        using LocalResidualBulk = GetPropType<TypeTag, Properties::LocalResidual>;
        using ElementBoundaryTypes = GetPropType<TypeTag, Properties::ElementBoundaryTypes>;
        LocalResidualBulk localResidualBulk(problem.get(), timeLoop.get());

        // find dof on inlet (Dirichlet BC)
//         static constexpr int dim = bulkGridView.dimension;
//         std::vector<bool> bulkIsOnInlet(bulkGridView.size(dim), false);
//         for (const auto& element : elements(bulkGridView))
//         {
//             auto fvGeometry = localView(*bulkFvGridGeometry);
//             fvGeometry.bind(element);
//
//             for (const auto& scv : scvs(fvGeometry))
//             {
//                 const auto& globalPosition = scv.center();
//                 if(globalPosition[2] <= 0.01)
//                 {
//                     if(fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex()))
//                     {
//                         for (const auto& scvf : scvfs(fvGeometry))
//                         {
// //                             const auto bcTypes = bulkProblem.boundaryTypes(element, scvf);
//                             /*if (bcTypes.hasDirichlet()) */bulkIsOnInlet[scv] = true;
//                         }
//                     }
//                 }
//             }
//         }

        NumEqVector inFluxBulk(0.0);
        NumEqVector storageBulk(0.0);
        NumEqVector sumReaction(0.0);
        for (const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(*gridGeometry);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVariables->curGridVolVars());
            elemVolVars.bind(element, fvGeometry, x);
            auto prevElemVolVars = localView(gridVariables->prevGridVolVars());
            prevElemVolVars.bind(element, fvGeometry, xOld);
            auto elemFluxVarsCache = localView(gridVariables->gridFluxVarsCache());
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            ElementBoundaryTypes elementBoundaryTypes;
            elementBoundaryTypes.update(*problem, element, fvGeometry);

            //calculate storage:
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];

                const auto& globalPosition = scv.center();
                if(globalPosition[2] <= 0.01)
                {
                    if(fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex()))
                    {
                        auto temp = localResidualBulk.evalStorage(element, fvGeometry, prevElemVolVars, elemVolVars);
                        temp += localResidualBulk.evalFluxAndSource(element, fvGeometry, elemVolVars, elemFluxVarsCache, elementBoundaryTypes);
                        inFluxBulk+=temp[scv.localDofIndex()];
//                         std::cout << "inFluxBulk " << inFluxBulk<< "\n";
                    }
                }

                if(globalPosition[2] <= 0.08)
                {
                    storageBulk[0 /*N2*/] += volVars.molarDensity(0/*phaseIdx*/)*volVars.porosity()
                                                *volVars.moleFraction(0/*phaseIdx*/, 0 /*compIdx*/)
                                                *scv.volume();
                    storageBulk[1 /*H2O*/] += volVars.molarDensity(0/*phaseIdx*/)*volVars.porosity()
                                                *volVars.moleFraction(0/*phaseIdx*/, 1 /*compIdx*/)
                                                *scv.volume();
                    storageBulk[2 /*CaO*/] += volVars.solidVolumeFraction(0/*cPhaseIdx*/)
                                                * volVars.solidComponentMolarDensity(0/*cPhaseIdx*/)*scv.volume();
                    storageBulk[3 /*CaO2H2*/] += volVars.solidVolumeFraction(1/*hPhaseIdx*/)
                                                * volVars.solidComponentMolarDensity(1/*hPhaseIdx*/)*scv.volume();
                    storageBulk[4 /*energy*/] += (volVars.porosity()
                                                * volVars.density(0/*phaseIdx*/)
                                                * volVars.internalEnergy(0/*phaseIdx*/)
                                                +
                                                volVars.temperature()
                                                * volVars.solidHeatCapacity()
                                                * volVars.solidDensity()
                                                * (1.0 - volVars.porosity()))
                                                *scv.volume();

                    sumReaction += problem->source(element, fvGeometry, elemVolVars, scv)*scv.volume();

                }
            }
        }

        // print out all the balances in the bulkproblem
        problem->outputBalances(inFluxBulk, storageBulk, sumReaction);

        xOld = x;

        // set new dt as suggested by the newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

        // write vtk output
        // TODO uncomment if statement to write out only at the end of an episode
//         if (timeLoop->isCheckPoint() || timeLoop->finished())
//         {
            vtkWriter.write(timeLoop->time());

            problem->updateVtkOutput(x);
//         }

    } while (!timeLoop->finished());

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
