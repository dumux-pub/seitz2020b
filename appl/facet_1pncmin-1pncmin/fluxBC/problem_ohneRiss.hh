// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetTests
 * \brief The problem for the bulk domain in the 1pnc without facet coupling.
 */
#ifndef DUMUX_TEST_ONEPNC_BULKPROBLEM_HH
#define DUMUX_TEST_ONEPNC_BULKPROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/material/fluidsystems/h2on2.hh>
#include <dumux/material/fluidsystems/1padapter.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1pncmin/model.hh>

#include <dumux/material/fluidmatrixinteractions/1p/thermalconductivityaverage.hh>
#include <dumux/material/components/cao2h2.hh>
#include <dumux/material/components/cao.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include "spatialparams_bulk.hh"
#include "thermochemreaction.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePNCBulkOhneRissProblem;

namespace Properties {

// create the type tag nodes
namespace TTag {
struct OnePNCBulkOhneRiss { using InheritsFrom = std::tuple<OnePNCMinNI>; };
struct OnePNCBulkOhneRissBox { using InheritsFrom = std::tuple<BoxModel, OnePNCBulkOhneRiss>; };
} // end namespace TTag

// Set the grid type (DIMWORLD is defined in CMakeLists.txt)
template<class TypeTag>
// old struct
// struct Grid<TypeTag, TTag::OnePNCBulkOhneRiss> { using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };
// use this with 3D grids
struct Grid<TypeTag, TTag::OnePNCBulkOhneRiss> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePNCBulkOhneRiss> { using type = OnePNCBulkOhneRissProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePNCBulkOhneRiss>
{
    using type = OnePNCMinNIBulkSpatialParams< GetPropType<TypeTag, Properties::GridGeometry>,
                                    GetPropType<TypeTag, Properties::Scalar> >;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePNCBulkOhneRiss>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2ON2 = FluidSystems::H2ON2<Scalar, FluidSystems::H2ON2DefaultPolicy</*simplified=*/true>>;
public:
    using type = FluidSystems::OnePAdapter<H2ON2, H2ON2::gasPhaseIdx>;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::OnePNCBulkOhneRiss>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Components::CaO<Scalar>;
    using ComponentTwo = Components::CaO2H2<Scalar>;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo>;
};

} // end namespace Properties

/*!
 * \ingroup FacetTests
 * \brief Test problem for the 1pnc model with
 *        coupling across the bulk grid facets.
 */
template<class TypeTag>
class OnePNCBulkOhneRissProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using GridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using ReactionRate = ThermoChemReactionPoro;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;

public:
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    enum
    {
        // Indices of the primary variables
        pressureIdx = Indices::pressureIdx,
//         N2Idx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::N2Idx),

        H2OIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::H2OIdx),
        CaOIdx = FluidSystem::numComponents,
        CaO2H2Idx = FluidSystem::numComponents+1,

        // Equation Indices
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase Indices
        phaseIdx = FluidSystem::phase0Idx,
        cPhaseIdx = SolidSystem::comp0Idx,

        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx
    };

    OnePNCBulkOhneRissProblem(std::shared_ptr<const GridGeometry> gridGeometry)
        : ParentType(gridGeometry)
    {
        //initialize fluid system
        //initialize fluid system
        FluidSystem::init(/*tempMin=*/363.15,
                          /*tempMax=*/823.15,
                          /*numTemptempSteps=*/30,
                          /*startPressure=*/1e5,
                          /*endPressure=*/3e5,
                          /*pressureSteps=*/100);

        // stating in the console whether mole or mass fractions are used
        if(getPropValue<TypeTag, Properties::UseMoles>())
            std::cout << "problem uses mole fractions" << std::endl;
        else
            DUNE_THROW(Dune::InvalidStateException, "Problem is implemented for molar formulation!");

        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" +
                         getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        inletPressure_= getParam<Scalar>("Problem.InletPressure");
        reactionRate_.resize(gridGeometry->numDofs());
        outputFile_.open("fluxbalances_ohneRiss.csv", std::ios::out);
        outputFile_ << "timeStepSize [s], BulkIfluxN2 [mol], BulInfluxH2O [mol], BulkInfluxEnthalpy [mol], storageN2 [mol], storageH2O [mol], storageCaO [mol], storageCa(OH)2 [mol], storageEnergy [J], sumReactionH2O [mol], sumReactionEnthalpy [J]"  << std::endl;
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Get the time step of the time loop
    void setTimeStep(Scalar t)
    { timeStep_ = t; }

    //! Specifies the type of boundary condition at a given position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (globalPos[2] < 1e-6 || globalPos[2] > this->gridGeometry().bBoxMax()[2] - 1e-6){
            values.setDirichlet(pressureIdx);
            values.setDirichlet(H2OIdx);
            values.setDirichlet(temperatureIdx);
//                         values.setAllDirichlet();
        }
//             values.setAllDirichlet();
        return values;
    }

    //! Specifies the type of interior boundary condition at a given position.
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Evaluates the source term at a given position.
    NumEqVector source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {

        ////// reaction source
        NumEqVector source(0.0);
        const auto& volVars = elemVolVars[scv];

        Scalar qMass = rrate_.thermoChemReactionSimple(volVars);
        Scalar  qMole =  qMass/FluidSystem::molarMass(H2OIdx)*(1-volVars.porosity());

//     //    make sure not more solid reacts than present
//     //    for CaO
       if (-qMole*timeStep_ + volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx) < 0 + eps_)
        {
            qMole = volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx)/timeStep_;
        }

        source[CaO2H2Idx] = qMole;
        source[CaOIdx] = - qMole;
        source[H2OIdx] = - qMole;

        Scalar deltaH = 108e3; // J/mol
        Scalar phi = volVars.porosity();
        source[energyEqIdx] = qMole * (deltaH - (phi/(1-phi))*(volVars.pressure(phaseIdx)/volVars.molarDensity(phaseIdx)));

        return source;
    }

    //! Evaluates the Dirichlet boundary condition for a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
//         values = initialAtPos(globalPos);

        if (globalPos[2] < 1e-6){
            values[temperatureIdx] = 573.15;
            values[H2OIdx] = 0.5;
            values[pressureIdx] = inletPressure_ ;//2.4e5;
        }

        if (globalPos[2] > this->gridGeometry().bBoxMax()[2] - 1e-6 ){
            values[temperatureIdx] = 573.15;
            values[H2OIdx] = 0.5;
            values[pressureIdx] = 2.0e5;
        }

        return values;
    }

    //! Evaluates the Neumann boundary condition for a boundary segment.
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    { return NumEqVector(0.0); }

    //! Evaluates the initial conditions.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        values[pressureIdx] = 2.0e5;
        values[H2OIdx] = 0.5;
        values[CaOIdx] = 0.2;
        values[CaO2H2Idx] = 0.0;
        values[temperatureIdx] = 573.15;

        if (globalPos[2] >= 0.08 ){
            values[CaO2H2Idx] = 0.01;
            values[CaOIdx] = 0.0001;
        }

        return values;
    }

    //! Returns the temperature in \f$\mathrm{[K]}\f$ in the domain.
    Scalar temperature() const
    { return 283.15; /*10°*/ }


     /*!
     * \brief Return the reaction rate
     */
    const std::vector<Scalar>& getRRate()
    {
        return reactionRate_;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                reactionRate_[dofIdxGlobal] = (1-volVars.porosity())*rrate_.thermoChemReactionSimple(volVars);

            }
        }
    }

    //! Called after every time step
    //! Output the total out flux balances in [ \textnormal{unit of conserved quantity} )]
    void outputBalances(NumEqVector& inFluxBulk, NumEqVector& storage, NumEqVector& sumReaction)
    {
        outputFile_ << timeStep_<<", " //[s]
                    << inFluxBulk[pressureIdx] *timeStep_<<", " //[mol]
                    << inFluxBulk[H2OIdx] *timeStep_<<", " // [mol]
                    << inFluxBulk[temperatureIdx]*timeStep_ <<", " //[mol]
                    << storage[pressureIdx] <<", "  //[mol]
                    << storage[H2OIdx] <<", " //[mol]
                    << storage[CaOIdx] <<", "  //[mol]
                    << storage[CaO2H2Idx] <<", "  //[mol]
                    << storage[temperatureIdx]<< ", " //[J]
                    << sumReaction[H2OIdx] *timeStep_ << ", "  //[mol]
                    << sumReaction[temperatureIdx] *timeStep_    //[J]
                    <<std::endl;
    }

private:
    std::string problemName_;
    ReactionRate rrate_;
    Scalar timeStep_;
    Scalar eps_ = 1.0e-6;
    std::ofstream outputFile_;
    std::vector<double> reactionRate_;
    Scalar inletPressure_;
};

} // end namespace Dumux

#endif
